package com.saurabh.newsexpress.profile;

import com.saurabh.newsexpress.R;
import com.saurabh.newsexpress.model.profile.Group;

import java.util.ArrayList;
import java.util.List;

public class ProfilePresenter implements ProfileContract.Presenter {

    private ProfileContract.View view;

    public ProfilePresenter(ProfileContract.View view) {
        this.view = view;
    }


    public void getGroupData() {
        List<Group> groupList = new ArrayList<>();

        Group group = new Group();
        group.setName("Dialogue");
        group.setImage(R.drawable.dialogue);

        Group group1 = new Group();
        group1.setName("Klick");
        group1.setImage(R.drawable.klick);

        Group group2 = new Group();
        group2.setName("Beauty");
        group2.setImage(R.drawable.beauty);

        Group group3 = new Group();
        group3.setName("More");
        group3.setImage(R.drawable.more);

        groupList.add(0, group);
        groupList.add(1, group1);
        groupList.add(2, group2);
        groupList.add(3, group3);

        view.setAdapterData(groupList);

    }

    public void getSettingData(){
        List<String> settingList = new ArrayList<>();
        settingList.add("WeMedia");
        settingList.add("Join WeMedia");
        settingList.add("Language");
        settingList.add("Setting");

        view.setSettingAdapter(settingList);
    }

}
