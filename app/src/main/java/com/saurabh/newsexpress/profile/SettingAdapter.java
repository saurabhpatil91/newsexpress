package com.saurabh.newsexpress.profile;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.saurabh.newsexpress.R;
import com.saurabh.newsexpress.util.AndroidUtils;

import java.util.ArrayList;
import java.util.List;

public class SettingAdapter extends RecyclerView.Adapter<SettingAdapter.ViewHolder> {

    private List<String> dataList = new ArrayList<>();
    private Context context;

    SettingAdapter(Context context) {
        this.context = context;
    }

    public void setData(List<String> dataList) {
        this.dataList = dataList;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    @Override
    public SettingAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.cell_setting, parent, false);
        return new SettingAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(SettingAdapter.ViewHolder holder, int position) {

        String s = dataList.get(position);
        AndroidUtils.setText(holder.settingName, s);

    }

    class ViewHolder extends RecyclerView.ViewHolder {


        TextView settingName;


        ViewHolder(View itemView) {
            super(itemView);

            settingName = itemView.findViewById(R.id.tv_name);

        }
    }

}
