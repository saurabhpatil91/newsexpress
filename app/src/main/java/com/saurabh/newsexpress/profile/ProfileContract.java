package com.saurabh.newsexpress.profile;

import com.saurabh.newsexpress.model.profile.Group;

import java.util.List;

public interface ProfileContract {
    interface View {
        void setAdapterData(List<Group> groupList);
        void setSettingAdapter(List<String> settingList);
    }

    interface Presenter {

        void getGroupData();
        void getSettingData();
    }
}
