package com.saurabh.newsexpress.profile;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.saurabh.newsexpress.R;
import com.saurabh.newsexpress.model.profile.Group;
import com.saurabh.newsexpress.util.AndroidUtils;
import com.saurabh.newsexpress.util.ImageLoadUtils;

import java.util.ArrayList;
import java.util.List;

public class GroupAdapter extends RecyclerView.Adapter<GroupAdapter.ViewHolder> {

    private List<Group> dataList = new ArrayList<>();
    private Context context;

    GroupAdapter(Context context) {
        this.context = context;
    }

    public void setData(List<Group> dataList) {
        this.dataList = dataList;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    @Override
    public GroupAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.cell_group, parent, false);
        return new GroupAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(GroupAdapter.ViewHolder holder, int position) {

        final Group group = dataList.get(position);
        AndroidUtils.setText(holder.tvGroupName, group.getName());
        ImageLoadUtils.setImage(context, group.getImage(), holder.ivGroupBg);

    }

    class ViewHolder extends RecyclerView.ViewHolder {

        ImageView ivGroupBg;
        TextView tvGroupName;


        ViewHolder(View itemView) {
            super(itemView);
            ivGroupBg = itemView.findViewById(R.id.iv_group);
            tvGroupName = itemView.findViewById(R.id.tv_group_name);

        }
    }

}
