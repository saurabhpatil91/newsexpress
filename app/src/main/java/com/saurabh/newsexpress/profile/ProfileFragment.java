package com.saurabh.newsexpress.profile;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.saurabh.newsexpress.R;
import com.saurabh.newsexpress.model.profile.Group;

import java.util.List;

public class ProfileFragment extends Fragment implements ProfileContract.View {

    public static final String TAG = ProfileFragment.class.getSimpleName();

    View view;
    RecyclerView rvGroupList;
    RecyclerView rvSettingList;
    GroupAdapter groupAdapter;
    SettingAdapter settingAdapter;
    ProfileContract.Presenter presenter;

    public static ProfileFragment newInstance() {
        return new ProfileFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_profile, container, false);

        rvGroupList = view.findViewById(R.id.rv_group_list);
        rvGroupList.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        groupAdapter = new GroupAdapter(getActivity());
        rvGroupList.setAdapter(groupAdapter);

        rvSettingList = view.findViewById(R.id.rv_setting);
        rvSettingList.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        settingAdapter = new SettingAdapter(getActivity());
        rvSettingList.setAdapter(settingAdapter);
        presenter = new ProfilePresenter(this);

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        presenter.getGroupData();
        presenter.getSettingData();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();

    }

    @Override
    public void setAdapterData(List<Group> groupList) {
        groupAdapter.setData(groupList);
    }

    @Override
    public void setSettingAdapter(List<String> settingList) {
        settingAdapter.setData(settingList);
    }
}
