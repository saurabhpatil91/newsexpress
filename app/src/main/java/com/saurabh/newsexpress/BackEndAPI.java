package com.saurabh.newsexpress;



import com.saurabh.newsexpress.model.headlines.HeadlineResponse;

import io.reactivex.Observable;
import io.reactivex.annotations.NonNull;
import retrofit2.Response;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Query;

public interface BackEndAPI {
    @Headers("Authorization:Bearer b78ac743d74d4132a3649f87bc57e355")
    @GET("/v2/top-headlines")
    Observable<Response<HeadlineResponse>> getArticles(@Query("country") String country, @NonNull @Query("category") String cat, @Query("page") String page);
}
