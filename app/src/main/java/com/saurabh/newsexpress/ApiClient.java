package com.saurabh.newsexpress;

import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiClient {

    private static final String BASE_URL = "https://newsapi.org/";
    public static final String APIKey = "b78ac743d74d4132a3649f87bc57e355";
    private static BackEndAPI backEndAPI = null;

    private ApiClient() {
    }

    public static BackEndAPI getBackEndAPI() {
        if (backEndAPI == null) {
            backEndAPI = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .build().create(BackEndAPI.class);
        }
        return backEndAPI;
    }
}
