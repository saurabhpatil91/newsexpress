package com.saurabh.newsexpress.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Created by soham on 6/11/17.
 */


public class TimeUtils {

    private static final int SECOND_MILLIS = 1000;
    private static final int MINUTE_MILLIS = 60 * SECOND_MILLIS;
    private static final int HOUR_MILLIS = 60 * MINUTE_MILLIS;
    private static final int DAY_MILLIS = 24 * HOUR_MILLIS;

    private static Date currentDate() {
        Calendar calendar = Calendar.getInstance();
        return calendar.getTime();
    }

    public static String getTimeAgo(String strDate) {


        Date date = null;
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.ENGLISH);
        try {
            date = format.parse(strDate);

            long time = date.getTime();
            if (time < 1000000000000L) {
                time *= 1000;
            }

            long now = currentDate().getTime();
            if (time > now || time <= 0) {
                return "in the future";
            }

            final long diff = now - time;
            if (diff < MINUTE_MILLIS) {
                return "seconds ago";
            } else if (diff < 2 * MINUTE_MILLIS) {
                return "a minute ago";
            } else if (diff < 60 * MINUTE_MILLIS) {
                return diff / MINUTE_MILLIS + " minutes ago";
            } else if (diff < 2 * HOUR_MILLIS) {
                return "an hour ago";
            } else if (diff < 24 * HOUR_MILLIS) {
                return diff / HOUR_MILLIS + " hours ago";
            } else if (diff < 48 * HOUR_MILLIS) {
                return "yesterday";
            } else {
                return diff / DAY_MILLIS + " days ago";
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return strDate;
    }

    public static String getDateByFormat(long millis, String dateFormat) {
        SimpleDateFormat format = new SimpleDateFormat(dateFormat, Locale.getDefault());
        return format.format(new Date(millis));
    }

    public static String getDateInRequiredFormat(String mStringDate, String oldFormat, String newFormat) {


        try {
            String formatedDate = "";
            SimpleDateFormat dateFormat = new SimpleDateFormat(oldFormat);
            Date myDate = null;
            try {
                myDate = dateFormat.parse(mStringDate);
            } catch (java.text.ParseException e) {
                e.printStackTrace();
            }

            SimpleDateFormat timeFormat = new SimpleDateFormat(newFormat);
            formatedDate = timeFormat.format(myDate);

            if (!TextUtils.isEmpty(formatedDate)) {
                return formatedDate;
            }

            return mStringDate;
        } catch (Exception e) {
            e.printStackTrace();
            return mStringDate;
        }
    }

}
