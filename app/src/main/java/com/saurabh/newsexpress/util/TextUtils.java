package com.saurabh.newsexpress.util;

public class TextUtils {
    public static boolean isEmpty(String string) {
        return string == null || string.isEmpty() || "null".equalsIgnoreCase(string) || "".equalsIgnoreCase(string);
    }
}
