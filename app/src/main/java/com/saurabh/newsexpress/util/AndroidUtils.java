package com.saurabh.newsexpress.util;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.saurabh.newsexpress.R;

public class AndroidUtils {

    public static void setTintColor(ImageView imageView, Context context, int colorId) {
        imageView.setColorFilter(ContextCompat.getColor(context, colorId), android.graphics.PorterDuff.Mode.MULTIPLY);
    }

    public static Drawable getImageDrawable(Context context, int drawable) {

        return context.getResources().getDrawable(drawable);
    }

    public static void setImageDrawable(Context context, Drawable drawable, ImageView imageView) {
        try {
            imageView.setImageDrawable(drawable);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static Fragment replaceFragment(Fragment newFrag, int containerID, FragmentManager fragmentManager, String tag) {

        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.setCustomAnimations(R.anim.slide_right2left, 0, 0, 0);
        if (newFrag.isAdded()) {
            fragmentTransaction.show(newFrag);
        } else {
            fragmentTransaction.replace(containerID, newFrag);
        }
        if (isValueNullOrEmpty(tag))
            fragmentTransaction.addToBackStack(tag);

        fragmentTransaction.commitAllowingStateLoss();


        return newFrag;
    }

    public static boolean isValueNullOrEmpty(String value) {
        boolean isValue = false;
        if (value == null || value.equals("")
                || value.equals("null") || value.trim().length() == 0) {
            isValue = true;
        }
        return isValue;
    }

    public static void clearBackStack(FragmentManager fragmentManager) {
        if (fragmentManager.getBackStackEntryCount() > 0) {
            FragmentManager.BackStackEntry first = fragmentManager.getBackStackEntryAt(0);
            fragmentManager.popBackStack(first.getId(), FragmentManager.POP_BACK_STACK_INCLUSIVE);
        }
    }

    public static void showView(View view) {
        if (view != null) {
            view.setVisibility(View.VISIBLE);
        }
    }

    public static void setText(TextView textView, String string) {
        if (textView != null && !TextUtils.isEmpty(string)) {
            showView(textView);
            textView.setText(string.trim());
        }else{
            textView.setText("-");
        }
    }

    public static void startActivity(Activity activity, Intent intent) {
        if (activity != null) {
            activity.startActivity(intent);
            activity.overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
        }
    }

}
