package com.saurabh.newsexpress.util;

import android.content.Context;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

public class ImageLoadUtils {

    public static void setImage(Context context , String url , ImageView imageView){
        Glide.with(context)
                .load(url)
                .into(imageView); //ImageView to set.
    }

    public static void setImage(Context context , int res , ImageView imageView){
        Glide.with(context)
                .load(res)
                .into(imageView); //ImageView to set.
    }
}
