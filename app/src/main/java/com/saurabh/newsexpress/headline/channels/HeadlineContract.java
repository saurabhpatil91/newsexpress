package com.saurabh.newsexpress.headline.channels;

import com.saurabh.newsexpress.model.headlines.Article;

import java.util.List;

public interface HeadlineContract {

    interface View {

        void setTotalCount(int totalResults);
        void setAdapterData(List<Article> articles);


    }

    interface Presenter {

        void doAPICall(String country, String category, String pageNo);
    }
}
