package com.saurabh.newsexpress.headline;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.saurabh.newsexpress.R;
import com.saurabh.newsexpress.headline.channels.HeadlineFragment;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class NewsFragment extends Fragment {
    public static final String INDIA = "";
    public static final String BUSINESS = "business";
    public static final String ENTERTAINMENT = "entertainment";

    public static final String TAG = NewsFragment.class.getSimpleName();
    public FragmentManager fragmentManager;

    ViewPager vpChannel;
    TabLayout tabChannel;
    Context context;
    View view;

    public static NewsFragment newInstance() {
        return new NewsFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        fragmentManager = Objects.requireNonNull(getActivity()).getSupportFragmentManager();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_news, container, false);

        vpChannel = view.findViewById(R.id.vp_channel);
        setupViewPager(vpChannel);

        tabChannel = view.findViewById(R.id.tab_channel);
        tabChannel.setupWithViewPager(vpChannel);
        setupTabIcons();

        return view;
    }

    private void setupTabIcons() {
        Objects.requireNonNull(tabChannel.getTabAt(0)).setText(R.string.tab1);
        Objects.requireNonNull(tabChannel.getTabAt(1)).setText(R.string.tab2);
        Objects.requireNonNull(tabChannel.getTabAt(2)).setText(R.string.tab3);

    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getChildFragmentManager());
        adapter.addFragment(HeadlineFragment.newInstance(INDIA), getActivity().getResources().getString(R.string.tab1));
        adapter.addFragment(HeadlineFragment.newInstance(BUSINESS), getActivity().getResources().getString(R.string.tab2));
        adapter.addFragment(HeadlineFragment.newInstance(ENTERTAINMENT), getActivity().getResources().getString(R.string.tab3));
        viewPager.setOffscreenPageLimit(2);
        viewPager.setAdapter(adapter);
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onDetach() {
        super.onDetach();

    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }


}
