package com.saurabh.newsexpress.headline.channels;

import android.util.Log;

import com.saurabh.newsexpress.ApiClient;
import com.saurabh.newsexpress.headline.NewsFragment;
import com.saurabh.newsexpress.model.headlines.HeadlineResponse;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Response;

public class HeadlinePresenter implements HeadlineContract.Presenter{

    private HeadlineContract.View view;

    public HeadlinePresenter(HeadlineContract.View view) {
        this.view = view;
    }


    public void doAPICall(String country, String category, String page) {
        Observable<Response<HeadlineResponse>> observable = ApiClient.getBackEndAPI().getArticles(country,category,page);
        if (observable != null) {
            observable.subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<Response<HeadlineResponse>>() {
                        @Override
                        public void onSubscribe(Disposable d) {

                        }

                        @Override
                        public void onNext(Response<HeadlineResponse> headlineResponse) {
                            Log.e("URL",String.valueOf(headlineResponse.raw().request().url()));
                            view.setTotalCount( headlineResponse.body().getTotalResults());
                            view.setAdapterData(headlineResponse.body().getArticles());

                        }

                        @Override
                        public void onError(Throwable e) {

                        }

                        @Override
                        public void onComplete() {

                        }
                    });
        }

    }
}
