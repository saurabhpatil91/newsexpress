package com.saurabh.newsexpress.headline.channels;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.saurabh.newsexpress.Constants;
import com.alibaba.android.arouter.launcher.ARouter;
import com.saurabh.newsexpress.R;
import com.saurabh.newsexpress.model.headlines.Article;
import com.saurabh.newsexpress.util.AndroidUtils;
import com.saurabh.newsexpress.util.BaseViewHolder;
import com.saurabh.newsexpress.util.CircleImageView;
import com.saurabh.newsexpress.util.ImageLoadUtils;
import com.saurabh.newsexpress.util.TimeUtils;

import java.util.ArrayList;
import java.util.List;

public class HeadlineAdapter extends RecyclerView.Adapter<BaseViewHolder> {

    private List<Article> mDataList = new ArrayList<>();
    private Context context;

    HeadlineAdapter(Context context) {
        this.context = context;
    }

    void addData(List<Article> dataList) {
        mDataList.addAll(dataList);
    }

    @Override
    public int getItemViewType(int position) {
        return mDataList.get(position).getType();
    }

    @Override
    public int getItemCount() {
        return mDataList.size();
    }

    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        switch (viewType) {
            case Constants.VIEW_TYPE_LOADING:
                return new FooterHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_loading, parent, false));
            default:
                return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.cell_headline, parent, false));
        }
    }

    @Override
    public void onBindViewHolder(@NonNull BaseViewHolder holder, int position) {
        holder.onBind(position);
    }

    void removeLoading() {
        if (!mDataList.isEmpty()) {
            int position = mDataList.size() - 1;
            Article article = mDataList.get(position);
            if (article != null && article.getType() == Constants.VIEW_TYPE_LOADING) {
                mDataList.remove(position);
                notifyItemRemoved(position);
            }
        }
    }

    void addLoading() {
        mDataList.add(new Article().setType(Constants.VIEW_TYPE_LOADING));
        notifyItemInserted(mDataList.size() - 1);
    }

    void clear() {
        mDataList.clear();
        notifyDataSetChanged();
    }

    class ViewHolder extends BaseViewHolder {

        ImageView ivHeadlinePhoto;
        CircleImageView civAuthorPhoto;
        TextView tvTitle;
        TextView tvAuthorName;
        TextView tvCreatedAt;
        ImageView ivCross;
        LinearLayout llArticle;

        ViewHolder(View itemView) {
            super(itemView);
            ivHeadlinePhoto = itemView.findViewById(R.id.iv_headline_photo);
            civAuthorPhoto = itemView.findViewById(R.id.iv_author);
            tvTitle = itemView.findViewById(R.id.tv_title);
            tvAuthorName = itemView.findViewById(R.id.tv_author_name);
            tvCreatedAt = itemView.findViewById(R.id.tv_created);
            ivCross = itemView.findViewById(R.id.iv_cross);
            llArticle = itemView.findViewById(R.id.ll_article);
        }

        @Override
        protected void clear() {

        }

        public void onBind(int position) {
            super.onBind(position);
            final Article article = mDataList.get(position);
            AndroidUtils.setText(tvCreatedAt, TimeUtils.getTimeAgo(article.getPublishedAt()));
            AndroidUtils.setText(tvTitle, article.getTitle());
            AndroidUtils.setText(tvAuthorName, article.getAuthor());
            ImageLoadUtils.setImage(context, article.getUrlToImage(), ivHeadlinePhoto);

            llArticle.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ARouter.getInstance().build("/news/detailActivity")
                            .withParcelable("article", article)
                            .navigation();
                }
            });
        }
    }

    public class FooterHolder extends BaseViewHolder {

        ProgressBar mProgressBar;

        FooterHolder(View itemView) {
            super(itemView);
            mProgressBar = itemView.findViewById(R.id.progressBar);
        }

        @Override
        protected void clear() {

        }

    }

}