package com.saurabh.newsexpress.headline.channels;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.alibaba.android.arouter.facade.annotation.Autowired;
import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.android.arouter.launcher.ARouter;
import com.saurabh.newsexpress.R;
import com.saurabh.newsexpress.model.headlines.Article;
import com.saurabh.newsexpress.util.AndroidUtils;
import com.saurabh.newsexpress.util.CircleImageView;
import com.saurabh.newsexpress.util.ImageLoadUtils;
import com.saurabh.newsexpress.util.TimeUtils;

@Route(path = "/news/detailActivity", name = "Detail Activity")
public class HeadlineDetailActivity extends AppCompatActivity implements View.OnClickListener {
    TextView tvAuthorName, tvAuthorNamePage, tvCreatedDate, tvHeadline, tvDescription;
    CircleImageView civAuthorPhoto;
    Button btnWhatsApp;
    ImageView back, ivNews;
    NestedScrollView nestedScrollView;
    @Autowired
    Article article;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_headline_detail);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ARouter.getInstance().inject(this);

        tvAuthorName = findViewById(R.id.tv_author_name);
        civAuthorPhoto = findViewById(R.id.civ_author_photo);

        back = findViewById(R.id.iv_back);
        tvAuthorNamePage = findViewById(R.id.tv_author_name_page);
        tvCreatedDate = findViewById(R.id.tv_created_page);
        nestedScrollView = findViewById(R.id.nested_scroll);
        btnWhatsApp = findViewById(R.id.btn_whatsapp);
        tvHeadline = findViewById(R.id.tv_headlines);
        tvDescription = findViewById(R.id.tv_description);
        ivNews = findViewById(R.id.iv_news);

        AndroidUtils.setText(tvAuthorName, article.getAuthor());
        AndroidUtils.setText(tvAuthorNamePage, article.getAuthor());
        AndroidUtils.setText(tvCreatedDate, article.getPublishedAt());
        AndroidUtils.setText(tvCreatedDate, TimeUtils.getDateInRequiredFormat(article.getPublishedAt(), "yyyy-MM-dd'T'HH:mm:ss'Z'", "dd-MMM HH:mm"));
        AndroidUtils.setText(tvHeadline, article.getTitle());
        AndroidUtils.setText(tvDescription, article.getContent());
        ImageLoadUtils.setImage(this, article.getUrlToImage(), ivNews);
        tvAuthorName.setVisibility(View.INVISIBLE);


        nestedScrollView.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
            @Override
            public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                if (scrollY <= 230) {
                    tvAuthorName.setVisibility(View.INVISIBLE);
                    civAuthorPhoto.setVisibility(View.INVISIBLE);
                } else {
                    tvAuthorName.setVisibility(View.VISIBLE);
                    civAuthorPhoto.setVisibility(View.VISIBLE);
                }
            }
        });


        btnWhatsApp.setOnClickListener(this);
        back.setOnClickListener(this);
    }


    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;

            case R.id.btn_whatsapp:
                Intent whatsappIntent = new Intent(Intent.ACTION_SEND);
                whatsappIntent.setType("text/plain");
                whatsappIntent.setPackage("com.whatsapp");
                whatsappIntent.putExtra(Intent.EXTRA_TEXT, article.getTitle() + " \n" + article.getUrl());
                try {
                    this.startActivity(whatsappIntent);
                } catch (android.content.ActivityNotFoundException ex) {
                    Toast.makeText(this, "WhatsApp have not been installed.", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }
}
