package com.saurabh.newsexpress.headline.channels;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.saurabh.newsexpress.R;
import com.saurabh.newsexpress.model.headlines.Article;
import com.saurabh.newsexpress.util.PaginationScrollListener;
import java.util.List;


public class HeadlineFragment extends Fragment implements HeadlineContract.View, SwipeRefreshLayout.OnRefreshListener {

    public static final int PAGE_START = 1;
    private static final String CATEGORY = "category";
    View view;
    RecyclerView rvHeadlines;
    SwipeRefreshLayout swipeRefreshLayout;
    HeadlineAdapter headlineAdapter;
    HeadlineContract.Presenter presenter;
    private int totalPage = 0;
    private int currentPage = PAGE_START;
    private boolean isLoading = false;
    private String category;

    public static HeadlineFragment newInstance(String category) {
        HeadlineFragment fragment = new HeadlineFragment();
        Bundle args = new Bundle();
        args.putString(CATEGORY, category);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            category = getArguments().getString(CATEGORY);
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_headline, container, false);
        rvHeadlines = view.findViewById(R.id.headlines);
        swipeRefreshLayout = view.findViewById(R.id.swipeToRefresh);
        presenter = new HeadlinePresenter(this);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        rvHeadlines.setLayoutManager(layoutManager);
        headlineAdapter = new HeadlineAdapter(getActivity());
        rvHeadlines.setAdapter(headlineAdapter);

        swipeRefreshLayout.setOnRefreshListener(this);

        rvHeadlines.addOnScrollListener(new PaginationScrollListener(layoutManager) {
            @Override
            protected void loadMoreItems() {
                isLoading = true;
                currentPage++;
                presenter.doAPICall("in", category, String.valueOf(currentPage));
            }

            @Override
            public boolean isLastPage() {
                return currentPage == totalPage;
            }

            @Override
            public boolean isLoading() {
                return isLoading;
            }

        });
        initialAPICall();
        return view;
    }

    private void initialAPICall() {
        currentPage = PAGE_START;
        headlineAdapter.clear();
        presenter.doAPICall("in", category, String.valueOf(PAGE_START));
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void setAdapterData(List<Article> articles) {

        if (currentPage != PAGE_START) {
            headlineAdapter.removeLoading();
        }
        headlineAdapter.addData(articles);
        swipeRefreshLayout.setRefreshing(false);
        if (currentPage < totalPage) {
            headlineAdapter.addLoading();
        }
        isLoading = false;
    }

    @Override
    public void setTotalCount(int totalResults) {
        int pages = totalResults / 20;
        if (totalResults % 20 == 0) {
            totalPage = pages;
        } else {
            totalPage = pages + 1;
        }

    }

    @Override
    public void onRefresh() {
        initialAPICall();
    }
}