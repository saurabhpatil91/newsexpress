package com.saurabh.newsexpress;


import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.Toast;

import com.saurabh.newsexpress.headline.NewsFragment;
import com.saurabh.newsexpress.profile.ProfileFragment;
import com.saurabh.newsexpress.util.AndroidUtils;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    static boolean doubleBackToExitPressedOnce = false;
    private static int tabNo = 0;
    FrameLayout container;
    ImageView news, profile;
    FragmentManager fragmentManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        container = findViewById(R.id.container);
        news = findViewById(R.id.news);
        profile = findViewById(R.id.profile);
        fragmentManager = getSupportFragmentManager();
        AndroidUtils.clearBackStack(fragmentManager);

        news.setOnClickListener(this);
        profile.setOnClickListener(this);
        AndroidUtils.replaceFragment(NewsFragment.newInstance(), R.id.container, fragmentManager, NewsFragment.TAG);

    }

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            this.finish();
            return;
        }
        doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.news:
                if (tabNo != 0) {
                    AndroidUtils.setImageDrawable(getApplicationContext(), AndroidUtils.getImageDrawable(getApplicationContext(), R.drawable.ic_newspaper_pink), news);
                    AndroidUtils.setImageDrawable(getApplicationContext(), AndroidUtils.getImageDrawable(getApplicationContext(), R.drawable.ic_user_black), profile);
                    AndroidUtils.replaceFragment(NewsFragment.newInstance(), R.id.container, fragmentManager, NewsFragment.TAG);
                    tabNo = 0;
                }
                break;

            case R.id.profile:
                if (tabNo != 1) {
                    AndroidUtils.setImageDrawable(getApplicationContext(), AndroidUtils.getImageDrawable(getApplicationContext(), R.drawable.ic_user_pink), profile);
                    AndroidUtils.setImageDrawable(getApplicationContext(), AndroidUtils.getImageDrawable(getApplicationContext(), R.drawable.ic_newspaper_black), news);
                    AndroidUtils.replaceFragment(ProfileFragment.newInstance(), R.id.container, fragmentManager, ProfileFragment.TAG);
                    tabNo = 1;
                }
                break;
        }
    }
}
